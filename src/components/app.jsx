import React from 'react'

import {Todos} from '../containers/todos'
import {Users} from './users'
import {Route, Redirect, Link, NavLink, Switch} from 'react-router-dom'

const Panel = props => {
    return <div className="row">
                <div className="col">
                    {props.children}
                </div>
            </div>
}

const Nav = props =>{
    return <ul className="nav nav-tabs mb-3">
        {props.children.map( link => {
            return <li key={link.props.name} className="nav-item" 
                        onClick={()=>props.onSelect(link.props.name)}>
                <a className={`nav-link ${(link.props.name == props.selected) && 'active' }` }>
                    {link.props.children} 
                </a>
            </li>
        })}
    </ul>
}

export class App extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            tab: 'users'
        }
    }

    selectTab(tab){
        this.setState({tab})
    }

    render(){
        return <div className="container">
            <Panel>
               <ul className="nav nav-tabs mb-3">
                    <li className="nav-item">
                        <NavLink className="nav-link" activeClassName="active" to="/users">Users</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" activeClassName="active" to="/todos">Todos</NavLink>
                    </li>
                </ul>
            </Panel>

        {/* {this.state.tab == 'todos'? <Redirect to="/users" /> : null} */}

            <Panel>
                <Switch>
                    <Route path="/" exact render={()=>
                        <Redirect to="/users" />
                    } />
                    <Route path="/users" component={Users} />
                    <Route path="/todos" component={Todos} />
                    <Route render={()=> <h1>404</h1>} />
                </Switch>
            </Panel>
        </div>
    }
}