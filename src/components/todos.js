import React from 'react';
import {TodosList} from './todoslist'

import {Route, NavLink, Redirect} from 'react-router-dom'

// import {store} from '../store'

function makeSelector(selector){
    var cache, prevEntities, prevList;

    return function(entities, list){
        
        if(!cache || entities != prevEntities || list != prevList){
            cache = selector(entities,list)
            prevEntities = entities;
            prevList = list
        }
        return cache
    }
}

const ArchiveItem = props => <div className="list-group-item">
 {props.data.title}
</div>

export class Todos extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            todo:{},
            todos: [],
            archived: [],
            tab:'todos'
        }

        this.todosSelector = makeSelector( (todo, todos) => todos.map( id => todo[id] ))
        this.archivedSelector = makeSelector( (todo, todos) => todos.map( id => todo[id] ))
    }

    // componentDidMount(){
    //     var state = store.getState();
    //     this.setState({
    //         todos: state.lists.todos,
    //         todo: state.entities.todo
    //     })
    //     this.unsubscribe = store.subscribe(()=>{
    //         var state = store.getState();
    //         this.setState({
    //             todos: state.lists.todos,
    //             todo: state.entities.todo
    //         })
            
    //     })
    // }
    // componentWillUnmount(){
    //     this.unsubscribe()
    // }

    toggleCompleted = (todo) => {

        const changedTodo = {
            ...todo,
            completed: !todo.completed
        }

        this.setState({
            todo:{
                ...this.state.todo,
                [changedTodo.id] : changedTodo
            }
        })
    }

    addTodo = (todoTitle) => {

        const newTodo = {
            id: Date.now(),
            title: todoTitle,
            completed: false
        }
        this.props.addTodo(newTodo)

        // this.setState({
        //     todo:{
        //         ...this.state.todo,
        //         [newTodo.id] : newTodo
        //     },
        //     todos: [ ...this.state.todos, newTodo.id ], // [].concat(this.state.todos, todo)
        // })
    }

    getTodos(){
         return this.todosSelector(this.state.todo, this.state.todos)
    }
    getArchived(){
         return this.archivedSelector(this.state.todo, this.state.archived)
    }

    archive = () => {
        this.props.todos.forEach( todo => {
            if(todo.completed){
                this.props.archiveTodo(todo)
            }
        })

    //     this.setState(this.getTodos()
    //         .reduce( (groups, todo)=>{
    //             groups[ todo.completed? 'archived' : 'todos'].push(todo.id)
    //             return groups
    //         },{
    //             todos:[],
    //             archived: [...this.state.archived]
    //         }))
    }

    removeTodo = (todo) => {
        // let index = this.state.todos.indexOf(todo.id)
        // let todos = this.state.todos.slice();
        // todos.splice(index,1)

        // this.setState({
        //     todos
        // })
    }

    selectTab(tab){
        this.setState({ tab })
    }

    render() {
        return <div> 
        <div className="row">                
                <div className="col">
                    <ul className="nav nav-tabs mb-3">
                        <li className="nav-item">
                            <NavLink to="/todos" exact activeClassName="active" className="nav-link">Todos</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/todos/archive"  activeClassName="active" className="nav-link">Archive</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="row">          

                <Route path="/todos" exact render={()=>
                    <div className="col">
                        <TodosList todos={this.props.todos}
                            toggleCompleted={this.props.todoToggle}
                            removeTodo={this.props.todoRemove}
                            addTodo={this.addTodo} />

                    <button onClick={this.archive}>Archive</button>
                    </div>
                } />

                <Route path="/todos/archive/" render={()=>
                <div className="col">
                    <TodosList title="Archive" todos={this.props.archived} 
                        Item={ArchiveItem}
                           addTodo={this.addTodo} 
                           removeTodo={this.props.todoRemove}
                           toggleCompleted={this.props.todoToggle} /> 
                </div>} />
            
                
            </div>
        </div>
    }
}  