import React from 'react'
//import {PropTypes} from 'react'
import PropTypes from 'prop-types'

const ListItem = props =>  <div className="list-group-item">
        <input type="checkbox" 
            checked={props.data.completed} 
            onChange={ () => props.toggleCompleted(props.data) } />
        {props.data.title}
        <div onClick={() => props.removeTodo(props.data)}> &times; </div>
</div>

export class TodosList extends React.PureComponent{

    static defaultProps = {
        // todos: [],
        // addTodo: () => {},
        title: 'Todos',
        Item: ListItem
    }

    static propTypes = {
        todos: PropTypes.array.isRequired,
        addTodo: PropTypes.func
    }

    constructor(props){
        super(props)
        this.state = {
            todoTitle: ''
        }
    }

    // componentWillMount(){
    //     console.log('componentWillMount', this.props.title)
    // }

    componentDidMount(){
        console.log('componentDidMount', this.refs)
       // this.refs['todoInput'].focus()
       this.input.focus()
    }
    componentWillUpdate( ){
        //console.log('update', this.props.title)
    }

    componentDidUpdate( ){
        //this.refs['todoInput'].style.border = '2px solid red '
    }

    // componentWillUnmount(){
    //     console.log('componentWillUnmount', this.props.title)
    // }

    // componentWillReceiveProps(newProps){
    //     console.log('componentWillReceiveProps', newProps, this.props, this.props.title)
    // }

    // shouldComponentUpdate( newProps, newState ){
    //     //console.log('componentWillReceiveProps', newProps, this.props, newState, this.state)
    //     return newProps.todos !== this.props.todos || this.state !== newState;
    // }
    
    inputChange = (event) => {
        // Synthetic Event ERror:
       //console.log(event.persist())
       var value = event.target.value
        this.setState(()=>{
            return {
                todoTitle: value
            }
        })
    }

    addOnEnter = (event) => {
        if(event.keyCode == 13){
            this.addTodo()
        }
    }

    addTodo = () => {
        this.props.addTodo(this.state.todoTitle)
        this.setState({
            todoTitle:''
        })
    }

    render(){
        return <div>
            <h3>{this.props.title}</h3>
            <div className="list-group">
                { this.props.todos.map( (todo,index) => 
                   <this.props.Item key={todo.id} data={todo} 
                            toggleCompleted={this.props.toggleCompleted}
                            removeTodo={this.props.removeTodo} />
                )}
            </div>
            <div className="input-group">
                <input className="form-control" ref={ elem => this.input = elem}
                        value={this.state.todoTitle} 
                        onChange={ this.inputChange }  
                        onKeyUp={ this.addOnEnter } />

                <button className="btn btn-default" onClick={ this.addTodo }>Add</button>
            </div>
        </div>
    }
}