import React from 'react'

export class UserForm extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            user: this.props.user
        }
    }

    componentWillReceiveProps(newProps){
        this.setState({
            user: newProps.user
        })
    }

    handleChange = (event) => {
        let user = {
            ...this.state.user,
            [event.target.name]: event.target.value
        }
        this.setState({user})
    }

    handleSubmit = event => {
        event.preventDefault();
        this.props.onSave(this.state.user)
    }

    render() {
        let user = this.state.user;
        return <form onSubmit={this.handleSubmit}>
            <div className="form-group">
                <label htmlFor="">Name</label>
                <input type="text"
                    className="form-control"
                    value={user.name}
                    name="name"
                    onChange={this.handleChange} />
            </div>
            <div className="form-group">
                <label htmlFor="">E-mail</label>
                <input type="text"
                    className="form-control"
                    value={user.email}
                    name="email"
                    onChange={this.handleChange} />
            </div>
            <div className="form-group">
                <label htmlFor="">Website</label>
                <input type="text"
                    className="form-control"
                    value={user.website}
                    name="website"
                    onChange={this.handleChange} />
            </div>
            <button className="btn btn-success float-right">Save</button>
        </form>
    }
}