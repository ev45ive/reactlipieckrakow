import React from 'react'
import propTypes from 'prop-types'

export const UsersList = props => <div>
    <button onClick={props.onFetch}>Fetch</button>
    <div className="list-group">
    {props.users.map( user => 
        <div key={user.id} 
            className={`list-group-item 
                        ${props.selected == user.id && 'active'}`}
            onClick={() => props.onSelect(user)}>
            <div> {user.name} </div>  
        </div>
    )}
 </div></div>

 UsersList.propTypes = {
     selected: propTypes.oneOfType([
         propTypes.number,
         propTypes.string
     ]),
    users: propTypes.arrayOf(propTypes.object)
 }

 UsersList.defaultProps = {
     users: []
 }