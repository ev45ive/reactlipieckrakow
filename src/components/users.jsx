import React from 'react';
import {UsersList} from '../containers/users'
import {UserForm} from '../containers/form'


import {Route, NavLink, Redirect} from 'react-router-dom'

export class Users extends React.Component{

    render(){
        return <div className="row">
            <div className="col">
                <Route path="/users/:id" render={ (router)=>
                   <UsersList onSelect={
                        user => {
                            router.history.push('/users/'+user.id)
                        }} />
                } />
            </div>
            <div className="col">
                 <Route path="/users/:id" component={UserForm} />
                 <Route path="/users" exact render={()=><p>Select User</p>} />
            </div>
        </div>  
    }
}