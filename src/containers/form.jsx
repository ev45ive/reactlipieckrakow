import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {UserForm as UserFormView} from '../components/user-form'
import {UsersSelector, userChange, userSave}  from '../reducers/users'

const mapStateToProps = (state) => ({
    users: state.entities.user
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    onSave: user => userSave(dispatch)(user)
}, dispatch);

export const UserForm = connect(
    mapStateToProps, 
    mapDispatchToProps,
    (state,dispach,own)=>({
        user: state.users[own.match.params.id] || 1
    })
)(props => <div>{props.user && <UserFormView {...props}/>}</div>);