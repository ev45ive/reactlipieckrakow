import {connect} from 'react-redux'
import {Todos as TodosView} from '../components/todos'
import {bindActionCreators} from 'redux'
import {
    todoAdd,
    todoArchive,
    todoRemove,
    todoToggle,
    TodosSelector,
    ArchivedSelector
} from '../reducers/todosReducer'

const mapStateToProps = state => ({
    todos: TodosSelector(state),
    archived: ArchivedSelector(state)
})

const mapDispatchToProps = dispatch => ({
    addTodo: todo => dispatch(todoAdd(todo)),
    archiveTodo: todo => dispatch(todoArchive(todo)),
    todoRemove: todo => dispatch(todoRemove(todo)),
    todoToggle: todo=> dispatch(todoToggle(todo))
})

// const mapDispatchToProps = dispach => bindActionCreators({
//     todoAdd,
//     archiveTodo
// })(dispach)

export const Todos = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TodosView)