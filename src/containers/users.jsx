import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {withRouter} from 'react-router-dom'

import {UsersList as UsersView} from '../components/users-list'
import {UsersSelector, userSelect, usersFetch} from '../reducers/users'

const mapStateToProps = (state) => ({
    users: UsersSelector(state),
   // selected: state.selection && state.selection.user
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    onSelect: userSelect,
    onFetch: () => usersFetch(dispatch)
}, dispatch);

export const UsersList = withRouter(connect(
    mapStateToProps, 
    mapDispatchToProps,
    (state,dispatch,own,merged)=>{

        return ({
        ...state,
        ...dispatch,
        selected: own.match.params.id || 1,
        onSelect: (user) => {
            dispatch.onSelect(user)
            own.onSelect(user)
        }
    })}
)(UsersView));