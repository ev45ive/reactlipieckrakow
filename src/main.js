import './style.css'

import React from 'react';
import ReactDOM from 'react-dom';

import {
        BrowserRouter as Router,
        Route
} from 'react-router-dom'

import {Provider} from 'react-redux'
import {store} from './store'

import {App} from './components/app'

ReactDOM.render(
<Router>
        <Provider store={store}>
                <App title="Witaj"/>
        </Provider>
</Router>, 
        document.getElementById('app'))

    