import { createActions, handleActions, combineActions } from 'redux-actions'

export const {
    todoAdd, 
    todoArchive,
    todoRemove, 
    todoToggle
} = createActions({
    'TODO_ADD': todo => todo,
    'TODO_ARCHIVE': todo => todo,
    'TODO_REMOVE': todo => todo,
    'TODO_TOGGLE': todo => todo
})

// // Action CREATOR
// export const todoAdd = (todo) => ({
//     type:"TODO_ADD",
//     payload: todo
// })
// export const todoArchive = (todo) => ({
//     type:"TODO_ARCHIVE",
//     payload: todo
// })
// export const todoArchive = (todo) => ({
//     type:"TODO_REMOVE",
//     payload: todo
// })

// ID List Reducer
export const todosListReducer = (state = [],action) =>{

    switch(action.type){
        case "TODO_ADD":
            return [ ...state, action.payload.id]
        
        case "TODO_ARCHIVE":
        case "TODO_REMOVE":{
            let index = state.findIndex(id => id == action.payload.id)
            return [...state.slice(0,index), ...state.slice(index+1)]
        }
    }
    return state
}


export const archivedListReducer = (state=[], action)=>{
    
    switch(action.type){
        case "TODO_ARCHIVE":
            return [ ...state, action.payload.id]
    }
    return state    
}

// Entity Map Reducer
export const todosEntityReducer = (state = {},action) =>{

    switch(action.type){
        case "TODO_TOGGLE":
            let todo = {
                ...action.payload,
                completed: !action.payload.completed
            }

            return { 
                ...state, 
                [ action.payload.id ]: todo
            }
        case "TODO_UPDATE":
        case "TODO_ADD":
            // Object.assign({}, state, )
            return { 
                ...state, 
                [ action.payload.id ]: action.payload
            }
    }
    return state
}

import { createSelector } from 'reselect'

const getTodoEntities = state => state.entities.todo
const getTodoList = state => state.lists.todos
const getArchivedList = state => state.lists.archived

export const TodosSelector = createSelector(
    getTodoEntities,
    getTodoList,
    (entities,list) => list.map( id => entities[id] )    
)

export const ArchivedSelector = createSelector(
    getTodoEntities,
    getArchivedList,
    (entities,list) => list.map( id => entities[id] )    
)