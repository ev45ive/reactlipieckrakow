import {createActions} from 'redux-actions'

export const {userSelect,userChange} = createActions({
    "USER_SELECT": user => user,
    "USER_CHANGE": user => user
}) 

// https://github.com/gaearon/redux-thunk
// https://github.com/redux-saga/redux-saga
// https://github.com/pburtchaell/redux-promise-middleware/blob/master/docs/introduction.md
// dispatch({type:'FOO', payload: fetch('url').then(resp=>resp.json())} )

export const usersFetch = (dispatch) => {
        fetch('http://localhost:3000/users/')
        .then( response => response.json())
        .then( users => {
            dispatch({
                type:'USERS_FETCH_SUCCESS',
                payload: users
            })
        }, (err) => {
            dispatch({
                type:'USERS_FETCH_ERROR',
                payload: err
            })

        })

        return {type:'USERS_FETCH_START'}
}

export const userSave = (dispatch) => user =>  {
        fetch('http://localhost:3000/users/' + user.id,{
            method: 'PUT',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }).then( () => {
            dispatch({type:'USER_SAVE_SUCCESS'})
            dispatch(usersFetch(dispatch))
        })
        return {type:'USER_SAVE_START'}
}

export const usersEntityReducer = (state = {},action) => {
 
    switch(action.type){
        case 'USERS_FETCH_SUCCESS':
            return {
                //...state,
                ...action.payload.reduce( (entities, user) => {
                    entities[user.id] = user
                    return entities
                },{})
            }
    }
    return state 
};


export const usersListReducer = (state = [] ,action) => {
 
    switch(action.type){
        case 'USERS_FETCH_SUCCESS':
            return [ ...action.payload.map( user => user.id ) ]
    }
    return state 
};

import { createSelector } from 'reselect'

const getUserEntities = state => state.entities.user
const getUserList = state => state.lists.users

export const UsersSelector = createSelector(
    getUserEntities,
    getUserList,
    (entities,list) => list.map( id => entities[id] )    
)