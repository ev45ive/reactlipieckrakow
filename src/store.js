import {createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

const initialState = {
    // objects by ID
    entities:{
        todo:{
            1: {id:1, title:'Make Store', completed:false},
            2: {id:2, title:'Learn Redux', completed:true},
            3: {id:3, title:'Connect with React', completed:true}
        },
        user:{
            1: { id: 1, name: "Alice", email: "alice@wonderland.com", website:"wonderland.com"},            
            2: { id: 2, name: "Bob", email: "bob@builder.com", website:"builder.com"},            
            3: { id: 3, name: "Cat", email: "cat@catwoman.com", website:"batman.com"}    
        }
    },
    // order by ID
    lists:{
        todos:[1,2],
        archived:[3],
        users:[1,2,3],
    },
    selection:null,

    counter: 0,
    items: [],
    nested:{
        counter2:0
    }
};

const counterReducer = (state = 0, action) => {
    switch(action.type){
        case "INCREMENT":
            return state + 1;
        case "DECREMENT":
            return state - 1;
        default:
            return state
    }
}

const itemsReducer = (state = [], action) => {
       switch(action.type){
        case "ITEM_ADD":{
            return [...state, action.payload]
        }
        case "ITEM_REMOVE":{
            let index = state.findIndex(item => item.id == action.payload.id)
            return [...state.slice(0,index), ...state.slice(index+1)]
        }
        case "ITEM_UPDATE":{
            let index = state.findIndex(item => item.id == action.payload.id)
            return [...state.slice(0,index), action.payload,  ...state.slice(index+1)]
        }
        default:
            return state
    }

}

import {todosEntityReducer, todosListReducer, archivedListReducer} from './reducers/todosReducer'
import {usersEntityReducer,usersListReducer} from './reducers/users'

const rootReducer = combineReducers({

    entities:combineReducers({
        todo: todosEntityReducer,
        user: usersEntityReducer,
    }),
    lists:combineReducers({
        todos: todosListReducer,
        archived: archivedListReducer,
        users: usersListReducer
    }),
    selection: (state = {},action)=>{
        switch(action.type){
            case 'USER_CHANGE':
            case 'USER_SELECT':
            return {
                ...state,
                user: action.payload.id
            }
        }
        return state
    },
    'counter': counterReducer,
    'items': itemsReducer,
    'nested':combineReducers({
        'counter2': counterReducer
    })
})

// const rootReducer = (state, action) => {
//     state.counter = counterReducer(state.counter,action)
//     state.items = itemsReducer(state.items,action)

//     switch(action.type){
//         default:
//             return state
//     }
// }

export const store = createStore(rootReducer,initialState, composeWithDevTools(
  //applyMiddleware(...middleware),
))

console.log(store)

// CONSOLE: 
store.subscribe( () => console.log('STATE: ', store.getState() ) )
// store.dispatch({type:'INCREMENT'})
// store.dispatch({type:'ITEM_ADD', payload: {id:1, name:'test 1'}})
// store.dispatch({type:'ITEM_ADD', payload: {id:2, name:'test 2'}})
// store.dispatch({type:'ITEM_UPDATE', payload: {id:1, name:'test 3'}})
// store.dispatch({type:'ITEM_REMOVE', payload: {id:2}})