var ExtractText = require('extract-text-webpack-plugin')
var HTMLWebpackPlugin = require('html-webpack-plugin')
var path = require('path')
module.exports = {
    entry: {
        main: './src/main.js'
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devtool: 'sourcemap',
    resolve:{
        extensions: ['.jsx','.js']
    },
    plugins:[
        new ExtractText({filename:'styles.css'}),
        new HTMLWebpackPlugin({template:'./src/index.html'})
    ],
    module: {
        rules: [
            { test: /\.jsx?$/, use: 'babel-loader' },
            //{ test: /\.jsx?$/, use: 'awesome-typescript-loader' },
            {
                test: /\.css$/, use: ExtractText.extract({
                    use: 'css-loader'
                })
            },
            {
                test: /\.(svg|jpg|png)$/, use: {
                    loader: 'file-loader',
                    options: {
                        name: 'assets/[name]-[hash].[ext]'
                    }
                }
            }
        ]
    },
    devServer:{
        historyApiFallback: true
    }
}